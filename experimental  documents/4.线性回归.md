#### 实验四 线性回归算法实验
##### 使用线性回归算法预测披萨直径与价格的关系
（一）建立数据集
实验中使用已有的披萨直径与价格关系的数据集，建立一个披萨直径与价格的线性回归模型。

![输入图片说明](QQ%E6%88%AA%E5%9B%BE20220808110002.png)

此时，我们希望使用线性回归方法，去预测12寸的披萨的价格是多少？

（1）首先我们可以使用散点图大致看一下数据的分布情况：


```
%matplotlib inline                   
#绘制直径与价格的散点图
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties 
font = FontProperties(fname=r"c:\windows\fonts\msyh.ttc", size=10)
def runplt(size=None):
    plt.figure(figsize=size)
    plt.title('匹萨价格与直径数据',fontproperties=font)
    plt.xlabel('直径（英寸）',fontproperties=font)
    plt.ylabel('价格（美元）',fontproperties=font)
    plt.axis([0, 25, 0, 25])
    plt.grid(True)
    return plt
plt = runplt()
X = [[6], [8], [10], [14], [18]]
y = [[7], [9], [13], [17.5], [18]]
plt.plot(X, y, 'k.')
plt.show()

```
![输入图片说明](QQ%E6%88%AA%E5%9B%BE20220808112744.png)

上图中X轴表示匹萨直径，Y轴表示匹萨价格。能够看出，匹萨价格与其直径正相关，这与我们的日常经验也比较吻合，自然是尺寸越大价格越贵。

(2)下面我们使用SK-Learn中的线性回归方法，建立线性回归模型，并且预测一下12寸的披萨的价格。
   
   在这里由于只有5组训练样本，所以我们不进行数据集划分，所有数据都用来训练。

##### 调用SK-Learn 线性回归算法包
```
from sklearn import linear_model         #调用sklearn中的linear_model模块进行线性回归。
import numpy as np                       #导入numpy计算库
model = linear_model.LinearRegression()  #调用线性回归方法
model.fit(X, y)                          #模型训练
print("截距为: ",model.intercept_)      #输出截距
print("权重值为: ", model.coef_)          #输出权重值
a = model.predict([[12]])                #模型预测
print("预测一张12英寸匹萨价格：{:.2f}".format(model.predict([[12]])[0][0]))
```
##### 运行程序，观察结果。

##### 我们建立了一个预测披萨直径与价格线性回归方程。
$$ y=1.96551724+0.9762931x $$
  
  用这个模型，可以计算不同直径的价格。例如7寸，13寸，20寸。

输入下列代码：
```
print("预测一张7英寸匹萨价格：{:.2f}".format(model.predict([[7]])[0][0]))
print("预测一张13英寸匹萨价格：{:.2f}".format(model.predict([[13]])[0][0]))
print("预测一张20英寸匹萨价格：{:.2f}".format(model.predict([[20]])[0][0]))
```
观察预测结果。

根据训练好的模型的预测值，我们可以画出线性回归的拟合曲线，这里我们任意选取0,10,14,25寸披萨的预测值画出拟合曲线。


```
plt = runplt()
plt.plot(X, y, 'k.')
X2 = [[0], [10], [14], [25]]
model = linear_model.LinearRegression()
model.fit(X,y)
y2 = model.predict(X2)
plt.plot(X2, y2, 'g-')
plt.show()
```

![输入图片说明](QQ%E6%88%AA%E5%9B%BE20220808151316.png)

绿色的曲线即为训练集数据建立的拟合曲线。

#### 思考：

我们建立的线性回归算法模型中：

$$ y=1.96551724+0.9762931x $$

1.96551724 和 0.9762931分别代表什么含义？



